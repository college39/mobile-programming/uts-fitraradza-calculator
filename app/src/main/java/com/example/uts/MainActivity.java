package com.example.uts;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView tvResult;

    Button btnOn, btnResult, btnDiv, btnMultiply, btnMin, btnPlus, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0;

    double result = 0, firstValue = 0, numberTemp = 0, secondValue = 0, currentResult = 0;

    String operatorSelected = "";

    Boolean hasOperator = false, hasTreeStep = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvResult = (TextView) findViewById(R.id.tv_result);
        btnOn = (Button)findViewById(R.id.btn_on);
        btnResult = (Button)findViewById(R.id.btn_result);
        btnDiv = (Button)findViewById(R.id.btn_div);
        btnMultiply = (Button)findViewById(R.id.btn_multiply);
        btnMin = (Button)findViewById(R.id.btn_min);
        btnPlus = (Button)findViewById(R.id.btn_plus);
        btn1 = (Button)findViewById(R.id.btn_1);
        btn2 = (Button)findViewById(R.id.btn_2);
        btn3 = (Button)findViewById(R.id.btn_3);
        btn4 = (Button)findViewById(R.id.btn_4);
        btn5 = (Button)findViewById(R.id.btn_5);
        btn6 = (Button)findViewById(R.id.btn_6);
        btn7 = (Button)findViewById(R.id.btn_7);
        btn8 = (Button)findViewById(R.id.btn_8);
        btn9 = (Button)findViewById(R.id.btn_9);
        btn0 = (Button)findViewById(R.id.btn_0);

        btnOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvResult.setText("");
                hasOperator = false;
                result = 0;
                firstValue = 0;
                numberTemp = 0;
                secondValue = 0;
                currentResult = 0;
            }
        });

        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleResult(operatorSelected);
                hasOperator = false;
                operatorSelected = "";
                secondValue = 0;
                firstValue = 0;
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOnClick("/");
            }
        });

        btnMultiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOnClick("*");
            }
        });

        btnMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOnClick("-");
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleOnClick("+");
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("1");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("2");
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("3");
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("4");
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("5");
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("6");
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("7");
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("8");
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("9");
            }
        });

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleTextView("0");
            }
        });
    }

    public void handleTextView(String value) {
        secondValue = 0;
        if (hasOperator) {
            tvResult.setText(value);
            hasOperator = false;
        } else {
            tvResult.append(value);
        }
    }

    public void handleOnClick(String operator) {
        hasOperator = true;
        if (operatorSelected != operator && operatorSelected != "") {
            handleResult(operatorSelected);
        } else {
            handleResult(operator);
        }
        operatorSelected = operator;
    }

    public void handleResult(String type){
        double value = 0;
        switch (type) {
            case "*":
                if (firstValue == 0) {
                    firstValue = Double.parseDouble(tvResult.getText().toString());
                } else {
                    if (secondValue == 0) {
                        secondValue = Double.parseDouble(tvResult.getText().toString());
                        tvResult.setText(String.valueOf(firstValue * secondValue).replace(".0", ""));
                    } else {
                        tvResult.setText(String.valueOf(Double.parseDouble(tvResult.getText().toString()) * secondValue).replace(".0", ""));
                    }
                    firstValue = Double.parseDouble(tvResult.getText().toString());
                }
                break;
            case "/":
                if (firstValue == 0) {
                    firstValue = Double.parseDouble(tvResult.getText().toString());
                } else {
                    if (secondValue == 0) {
                        secondValue = Double.parseDouble(tvResult.getText().toString());
                        tvResult.setText(String.valueOf(firstValue / secondValue).replace(".0", ""));
                    } else {
                        tvResult.setText(String.valueOf(Double.parseDouble(tvResult.getText().toString()) / secondValue).replace(".0", ""));
                    }
                    firstValue = Double.parseDouble(tvResult.getText().toString());
                }
                break;
            case "+":
                if (firstValue == 0) {
                    firstValue = Double.parseDouble(tvResult.getText().toString());
                } else {
                    if (secondValue == 0) {
                        secondValue = Double.parseDouble(tvResult.getText().toString());
                        tvResult.setText(String.valueOf(firstValue + secondValue).replace(".0", ""));
                    } else {
                        tvResult.setText(String.valueOf(Double.parseDouble(tvResult.getText().toString()) + secondValue).replace(".0", ""));
                    }
                    firstValue = Double.parseDouble(tvResult.getText().toString());
                }
                break;
            case "-":
                if (firstValue == 0) {
                    firstValue = Double.parseDouble(tvResult.getText().toString());
                } else {
                    if (secondValue == 0) {
                        secondValue = Double.parseDouble(tvResult.getText().toString());
                        tvResult.setText(String.valueOf(firstValue - secondValue).replace(".0", ""));
                    } else {
                        tvResult.setText(String.valueOf(Double.parseDouble(tvResult.getText().toString()) - secondValue).replace(".0", ""));
                    }
                    firstValue = Double.parseDouble(tvResult.getText().toString());
                }
                break;
        }
    }
}